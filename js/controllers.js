var controllerModule = angular.module("ControllerModule", [])


controllerModule.controller('BookAddCtrl', function($scope, $http, $state, $stateParams, Books) {
    //books = Books.books;
    $scope.addBook = function(book){

        console.log(book);
        book.price = '0';    
        Books.add(book);
        console.log(books);
        $state.go('booklist', {bookType:book.type});
    };    

});


controllerModule.controller('BookDeleteCtrl', function($scope, $http, $state, $stateParams, $rootScope, Books) {
    $scope.deleteBook = function(id){
        Books.delete(id);
        $state.go('booklist', {bookType:$rootScope.bookType}, { reload: true });
    }
});


controllerModule.controller('BookDetailCtrl', function($scope, $state, $stateParams, Books) {
    $scope.getBookById = function(id){
        Books.get(id);
    }
    console.log($stateParams);
    $scope.bookInfo = Books.get($stateParams.bookId);
});



controllerModule.controller('BookListCtrl', function($scope, $http, $state, $stateParams, $rootScope, Books) {

	books = Books.books;

    $rootScope.bookType = $stateParams.bookType;

	console.log("in BookListCtrl controller");

    /////////////////////////

    function filterByType(obj) {
        if (obj.type == $stateParams.bookType || $stateParams.bookType == 0  ) {
            return true;
        } else {
            return false;
        }
    }


    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: false
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [5, 10],
        pageSize: $rootScope.pageSize,
        currentPage: 1
    };
    $scope.setPagingData = function(data, page, pageSize) {
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.books = pagedData;
        $scope.totalServerItems = data.length;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    console.log($stateParams);
    $scope.getPagedDataAsync = function(pageSize, page, searchText) {
        setTimeout(function() {
            $scope.setPagingData(books.filter(filterByType), page, pageSize);
            console.log(books);
        }, 100);
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function(newVal, oldVal) {
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        $rootScope.pageSize = $scope.pagingOptions.pageSize;
        console.log($rootScope.pageSize);        
        //console.log("watching");
    }, true);

    $scope.$watch('filterOptions', function(newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.gridOptions = {
        data: 'books',
        rowTemplate: '<div style="height: 100%"><div ng-style="{ \'cursor\': row.cursor }" ng-repeat="col in renderedColumns" ng-class="col.colIndex()" class="ngCell ">' +
            '<div class="ngVerticalBar" ng-style="{height: rowHeight}" ng-class="{ ngVerticalBarVisible: !$last }"> </div>' +
            '<div ng-cell></div>' +
            '</div></div>',
        multiSelect: false,
        enableCellSelection: true,
        enableRowSelection: false,
        enableCellEdit: true,
        enablePinning: true,
        columnDefs: [{
            field: 'index',
            displayName: 'No.',
            width: 60,
            pinnable: false,
            sortable: false
        }, {
            field: 'name',
            displayName: 'Book',
            enableCellEdit: false
        }, {
            field: 'author',
            displayName: 'Author',
            enableCellEdit: false,
            width: 220
        }, {
            field: 'pubTime',
            displayName: 'publish Date',
            enableCellEdit: false,
            width: 120
        }, {
            field: 'price',
            displayName: 'Price',
            enableCellEdit: true,
            width: 120,
            cellFilter: 'currency:"$"'
        }, {
            field: 'bookId',
            displayName: 'Operation',
            enableCellEdit: false,
            sortable: false,
            pinnable: false,
            cellTemplate: 'tpls/bookCellTemplate.html'


        }],
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,


        showFilter: true

    };

	//console.log(books);
});

