var bookStoreApp = angular.module('BookStoreApp', ['ui.router', 'ServicesModule', 'ControllerModule','ngGrid', 'DirectiveModule']);


bookStoreApp.run(function($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.pageSize = 5;
    $rootScope.bookType = 0;
    //Books.init();
    console.log('i am in run');
});


bookStoreApp.config(function($stateProvider, $urlRouterProvider){
	$urlRouterProvider.otherwise('/index');

	$stateProvider
		.state('index', {
			url: '/index',
			views: {
				'':{
					templateUrl: 'tpls/home.html'
				},
				'main@index': {
					templateUrl: 'tpls/loginForm.html'
				}

			}
		})
        .state('booklist', {
            url: '/{bookType:[0-9]{1,2}}',
            views: { // how multiple ui-views works
                '': {
                    templateUrl: 'tpls/bookList.html'
                },
                'booktype@booklist': {
                    templateUrl: 'tpls/bookType.html'
                },
                'bookgrid@booklist': {
                    templateUrl: 'tpls/bookGrid.html'
                }
            }
        })
        .state('addbook', {
            url: '/addbook',
            templateUrl: 'tpls/addBookForm.html'
        })
        .state('bookdetail', {
            url: '/bookdetail/:bookId', //pass parameter in router
            templateUrl: 'tpls/bookDetail.html'
        })          

	console.log('in config');

});